#pragma once
#include "Utils.hpp"
#include "MessageCoder.hpp"
#include "stdafx.h"

MessageCoder::MessageCoder(std::string originalMessage, std::string key) {
	this->mKey = key;
	this->mOriginalMessage = originalMessage;
	this->setKeyString();
	this->cipher();
}

std::string MessageCoder::getResult() {
	return this->mResult;
}

void MessageCoder::cipher() {
	mResult = "";
	for (size_t i = 0; i < mOriginalMessage.size(); i++) {
		mResult += replace(mOriginalMessage.at(i), mKeyString.at(i));
	}
}

void MessageCoder::setKeyString() {
	mKeyString = "";
	size_t limit = this->mKey.size();
	for (size_t i = 0; i < this->mOriginalMessage.size(); i++) {
		mKeyString += mKey.at(i % limit);
	}
}

char MessageCoder::replace(char original, char key) {
	char expected = original + key - 'A';
	if (expected > 'Z') expected = expected - 'Z' + 'A' -1;
	return expected;
}

std::string MessageCoder::getKeyString() {
	return mKeyString;
}