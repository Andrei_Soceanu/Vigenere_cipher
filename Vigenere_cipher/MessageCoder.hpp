#pragma once
#include "Utils.hpp"
#include "stdafx.h"

class MessageCoder {
private:
	/** Input message to be ciphered
	*/
	std::string mOriginalMessage;

	/** Key used to cipher mOriginalMessage
	*/
	std::string mKey;

	/** Output ciphered message
	*/
	std::string mResult;
	std::string mKeyString;
	/**
	* Replaces a character with the key offset
	* @param original the char to be replaced
	* @param key the char determining the offset
	* return the result char after applying offset
	*/
	void cipher();

	/**
	* Sets mResult using mKey and mOriginalMessage
	*/
	char replace(char original, char key);


	void setKeyString();

public:
	/**
	* Class constructor
	* @param originalMessage input message to be ciphered
	* @param key the key to cipher the message with
	*/
	MessageCoder(std::string originalMessage, std::string key);

	/**
	*
	* @return a string representing the ciphered message
	*/
	std::string getResult();
	std::string getKeyString();

};
