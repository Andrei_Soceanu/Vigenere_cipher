/* File:   Utils.hpp
* Author: Andrei Soceanu
* Created on October 27, 2017, 12:18 AM
*/
#pragma once
#include <iostream>
#include <string>
#include <algorithm>
#include "stdafx.h"
#include <string>

std::string toUpperCase(std::string input);
