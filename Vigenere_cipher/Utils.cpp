/* File:   Utils.cpp
* Author: Andrei Soceanu
* Created on October 26, 2017, 11:37 PM
*/

#include "Utils.hpp"
#include "stdafx.h"
#include <string>

std::string toUpperCase(std::string input) {
	std::string output = input;
	std::transform(output.begin(), output.end(), output.begin(), ::toupper);
	return output;
}

