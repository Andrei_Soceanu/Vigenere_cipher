// Vigenere_cipher.cpp : Defines the entry point for the console application.
//
#include "Utils.hpp"
#include "MessageCoder.hpp"
#include "stdafx.h"
#define WINDOWS 1
int main(int argc, char** argv) {
	while (true) {
		std::cout << "*************************************************************************************" << std::endl << std::endl;
		std::cout << "Enter the original message (will be converted to UPPER CASE) you want to cipher then press ENTER, or type EXIT to exit." << std::endl << std::endl;
		std::cout << "Message: ";
		std::string * input = new std::string();
		std::getline(std::cin, *input);
		*input = toUpperCase(*input);
		input->erase(std::remove_if(input->begin(), input->end(), ::isspace), input->end());
		if (*input == "EXIT") return 0;
		std::cout << "\nEnter the key (no spaces, will be converted to UPPER CASE) then press ENTER.\n" << std::endl;
		std::cout << "Key: ";
		std::string * key = new std::string();
		std::cin >> *key;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		*key = toUpperCase(*key);
		key->erase(std::remove_if(key->begin(), key->end(), ::isspace), key->end());
		std::cout << "\n\nOriginal message:\t" << *input << std::endl;
		MessageCoder * messageCoder = new MessageCoder(*input, *key);
		delete input;
		delete key;
		std::cout << "Ciphered message:\t" << messageCoder->getResult() << std::endl << std::endl << std::endl;
		delete messageCoder;
		#ifdef WINDOWS
		system("pause");
		system("cls");
		#endif
	}
	return 0;
}
